import React from 'react';
// import { FaGithub } from 'react-icons/fa';

import './style.scss';

// import jesuskataIcon from '../images/jesuskata-icon.png';
// import bulmaLogo from '../images/bulma-logo.png';
import Navbar from './navbar';

const Header = ({ siteTitle }) => (
  <section className="hero is-warning is-bold is-fullheight-with-navbar">
    <Navbar />
    <div className="hero-body">
      <div className="container center">
        <article className="media">
          {/* <figure className="is-left" style={{ paddingRight: 10 }}>
            <span className="icon is-large ">
              <img src={jesuskataIcon} alt="gatsby-logo" />
            </span>
          </figure> */}
          <div className="media-content">
            <div className="content">
              <h1 className="is-uppercase is-size-1 has-text-dark">
                jesuskata
              </h1>
              <p className="subtitle has-text-dark is-size-3">
                Bienvenido a mi Blog Personal
              </p>
            </div>
          </div>
        </article>
      </div>
    </div>
  </section>
);

export default Header;
