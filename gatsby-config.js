module.exports = {
  siteMetadata: {
    title: 'Blog Personal de Jesus Romero (jesuskata).',
    author: 'Jesus Romero',
    logo: 'http://app.jesuskata.com/wp-content/uploads/2019/04/logo-jesuskata.png',
    imageUrl: 'https://i.imgur.com/Vz81GEl.png',
    description: 'Este es el blog personal de Jesus Romero',
    keywords: 'Web developer, Web, Developer, Energia Solar, Diseño Gráfico, jesuskata, Huajuapan de Leon, Oaxaca, Mexico', // eslint-disable-line
    twitter: 'https://twitter.com/alx_kata',
    github: 'https://github.com/amandeepmittal',
    medium: 'https://medium.com/@amanhimself',
    gatsby: 'https://www.gatsbyjs.org/',
    bulma: 'https://bulma.io/',
    siteUrl: 'https://www.example.com'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`
      }
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Makefolio',
        short_name: 'Makefolio',
        start_url: '/',
        background_color: '#2980b9',
        theme_color: '#2980b9',
        display: 'standalone',
        icon: 'src/images/jesuskata-icon.png',
        orientation: 'portrait'
      }
    },
    {
      resolve: 'gatsby-source-wordpress',
      options: {
        // I have created a dummy site for us to use with the plugins we discussed
        baseUrl: 'app.jesuskata.com',
        protocol: 'https',
        hostingWPCOM: false,
        // We will be using some advanced custom fields
        useACF: true,
        acfOptionPageIds: [],
        verboseOutput: false,
        perPage: 100,
        searchAndReplaceContentUrls: {
          sourceUrl: 'https://app.jesuskata.com',
          replacementUrl: 'https://localhost:8000'
        },
        // Set how many simultaneous requests are sent at once.
        concurrentRequests: 10,
        includedRoutes: [
          '**/categories',
          '**/posts',
          '**/pages',
          '**/media',
          '**/tags',
          '**/taxonomies',
          '**/users'
        ],
        excludedRoutes: [],
        normalizer: function({ entities }) {
          return entities;
        }
      }
    },
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-plugin-google-analytics',
      options: {
        trackingId: 'UA-XXXXXXXX-X',
        // Setting this parameter is optional (requried for some countries such as Germany)
        anonymize: true
      }
    },
    'gatsby-plugin-sitemap'
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ]
};
